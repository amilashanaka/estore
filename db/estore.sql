-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 07, 2021 at 04:41 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `estore`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `a_id` int(11) NOT NULL AUTO_INCREMENT,
  `a_username` varchar(250) NOT NULL,
  `a_password` varchar(250) NOT NULL,
  `a_name` varchar(250) NOT NULL,
  `a_sec_key` varchar(250) DEFAULT NULL,
  `a_type` int(11) NOT NULL,
  `a_registered_by` int(11) DEFAULT NULL,
  `a_register_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `a_updated_by` int(11) DEFAULT NULL,
  `a_updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `a_type1_by` int(11) DEFAULT '0',
  `a_type2_by` int(11) DEFAULT '0',
  `a_type3_by` int(11) DEFAULT '0',
  `a_type4_by` int(11) DEFAULT '0',
  `a_type5_by` int(11) DEFAULT '0',
  `a_upline` int(11) DEFAULT '0',
  `a_currency` int(11) NOT NULL DEFAULT '0',
  `a_bank_name` varchar(255) DEFAULT NULL,
  `a_bank_account_no` varchar(255) DEFAULT NULL,
  `a_bank_branach` varchar(255) DEFAULT NULL,
  `a_address` varchar(255) DEFAULT NULL,
  `a_country` varchar(255) DEFAULT NULL,
  `a_city` varchar(255) DEFAULT NULL,
  `a_phone` varchar(255) DEFAULT NULL,
  `a_email` varchar(255) DEFAULT NULL,
  `a_state` varchar(255) DEFAULT NULL,
  `a_ref` int(11) DEFAULT '0',
  `a_img` varchar(255) DEFAULT NULL,
  `a_lang` varchar(10) DEFAULT NULL,
  `a_last_log` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `a_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`a_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`a_id`, `a_username`, `a_password`, `a_name`, `a_sec_key`, `a_type`, `a_registered_by`, `a_register_date`, `a_updated_by`, `a_updated_date`, `a_type1_by`, `a_type2_by`, `a_type3_by`, `a_type4_by`, `a_type5_by`, `a_upline`, `a_currency`, `a_bank_name`, `a_bank_account_no`, `a_bank_branach`, `a_address`, `a_country`, `a_city`, `a_phone`, `a_email`, `a_state`, `a_ref`, `a_img`, `a_lang`, `a_last_log`, `a_status`) VALUES
(1, 'admin', '$2y$10$MCq3kqg5TpP5rvviemVayuO4Hvfxh3/JJ4mylf6IsX7rhT3gagTee', 'SuperAdmin', '4143', 1, 0, '2020-04-14 21:59:24', 0, '2020-04-15 02:56:23', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'en', '2021-09-03 10:16:12', 1),
(2, 'seller', '$2y$10$EauaIIv4dX9aJhH95Bk3EuqrUKJwiNBcFH/60DbqfRXQ5jUAy1P0G', 'ADMINUSD', NULL, 2, 1, '2020-06-18 00:00:00', NULL, '2020-06-18 12:47:38', 1, 0, 0, 0, 0, 1, 2, NULL, NULL, NULL, NULL, 'Malaysia', 'KL', NULL, NULL, NULL, 8517, NULL, NULL, '2020-06-18 12:47:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_types`
--

DROP TABLE IF EXISTS `admin_types`;
CREATE TABLE IF NOT EXISTS `admin_types` (
  `at_id` int(11) NOT NULL AUTO_INCREMENT,
  `at_name` varchar(100) NOT NULL,
  `at_level` varchar(50) NOT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_types`
--

INSERT INTO `admin_types` (`at_id`, `at_name`, `at_level`) VALUES
(1, 'supermaster', '1'),
(2, 'Admin', '2'),
(3, 'SalePoint', '3'),
(4, 'Packing', '4'),
(5, 'Delivery', '5');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
CREATE TABLE IF NOT EXISTS `brand` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `brand_name`) VALUES
(1, 'Dare to Dazzle'),
(2, 'Diamond Dreams'),
(3, 'Rings and Bling'),
(4, 'Cool Jewels');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`) VALUES
(1, 'Anklet'),
(2, 'Bangle'),
(3, 'Bracelet'),
(4, 'Ring');

-- --------------------------------------------------------

--
-- Table structure for table `collection`
--

DROP TABLE IF EXISTS `collection`;
CREATE TABLE IF NOT EXISTS `collection` (
  `collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`collection_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collection`
--

INSERT INTO `collection` (`collection_id`, `collection_name`) VALUES
(1, 'Mens'),
(2, 'Women'),
(3, 'Weddings');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency` (
  `cu_id` int(11) NOT NULL AUTO_INCREMENT,
  `cu_name` varchar(100) NOT NULL,
  `cu_rate` decimal(13,8) NOT NULL,
  `cu_withdraw_rate` decimal(13,8) NOT NULL,
  `cu_symbol` varchar(5) DEFAULT NULL,
  `cu_created_by` int(11) NOT NULL,
  `cu_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cu_updated_by` int(11) DEFAULT NULL,
  `cu_updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cu_status` int(11) NOT NULL DEFAULT '1',
  `cu_bank` text,
  PRIMARY KEY (`cu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`cu_id`, `cu_name`, `cu_rate`, `cu_withdraw_rate`, `cu_symbol`, `cu_created_by`, `cu_created_date`, `cu_updated_by`, `cu_updated_date`, `cu_status`, `cu_bank`) VALUES
(1, 'USD ($)', '1.00000000', '1.00000000', '$', 1, '2020-04-16 00:00:00', 1, '2020-04-19 13:03:53', 1, 'CIMB ACC XXXXXXXXXXXXXXXXXXX  '),
(2, 'TH', '23.00000000', '12.00000000', 'à¸¿', 1, '2020-04-19 12:51:55', 1, '2020-06-18 00:00:00', 1, '23');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_sku` varchar(50) DEFAULT NULL,
  `p_ref_code` varchar(20) DEFAULT NULL,
  `p_name` longtext,
  `p_qty` float DEFAULT NULL,
  `P_short_des` longtext,
  `p_long_des` longtext,
  `p_category` int(255) DEFAULT '0',
  `p_brand` int(255) DEFAULT '0',
  `p_collection` int(255) DEFAULT '0',
  `p_related_products` int(11) DEFAULT NULL,
  `p_img_1` text,
  `p_img_2` text,
  `p_img_3` text,
  `p_img_4` text,
  `p_img_5` text,
  `p_img_6` text,
  `p_sales_price` decimal(25,12) DEFAULT '0.000000000000',
  `p_currency` int(11) DEFAULT '0',
  `p_cost` decimal(25,12) DEFAULT '0.000000000000',
  `p_discount` decimal(25,12) DEFAULT '0.000000000000',
  `p_status` varchar(11) DEFAULT NULL,
  `P_register_date` date DEFAULT NULL,
  `p_expire_date` date DEFAULT NULL,
  `p_created_by` int(11) DEFAULT NULL,
  `p_created_date` date DEFAULT NULL,
  `p_updated_by` int(11) DEFAULT NULL,
  `p_updated_date` date DEFAULT NULL,
  `p_mlm` int(11) DEFAULT '0',
  `p_supplier` int(11) DEFAULT '0',
  `p_seller` int(11) DEFAULT '0',
  `p_stock` int(11) DEFAULT '0',
  `p_stock_date` date DEFAULT NULL,
  `p_stock_updated_date` date DEFAULT NULL,
  `p_barcode` varchar(255) DEFAULT NULL,
  `p_tags` varchar(255) DEFAULT NULL,
  `p_video` text,
  PRIMARY KEY (`p_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `p_sku`, `p_ref_code`, `p_name`, `p_qty`, `P_short_des`, `p_long_des`, `p_category`, `p_brand`, `p_collection`, `p_related_products`, `p_img_1`, `p_img_2`, `p_img_3`, `p_img_4`, `p_img_5`, `p_img_6`, `p_sales_price`, `p_currency`, `p_cost`, `p_discount`, `p_status`, `P_register_date`, `p_expire_date`, `p_created_by`, `p_created_date`, `p_updated_by`, `p_updated_date`, `p_mlm`, `p_supplier`, `p_seller`, `p_stock`, `p_stock_date`, `p_stock_updated_date`, `p_barcode`, `p_tags`, `p_video`) VALUES
(1, '1', NULL, NULL, NULL, NULL, NULL, 2, 2, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.000000000000', 100, '100.000000000000', '100.000000000000', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '100', NULL),
(2, NULL, NULL, 'New product', 2, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000000000', 0, '0.000000000000', '0.000000000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, 'New product', 5, NULL, NULL, 3, 2, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000000000', 0, '0.000000000000', '0.000000000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, 'product1', 5, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000000000', 0, '0.000000000000', '0.000000000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(5, NULL, NULL, 'product2', 5, NULL, NULL, 1, 4, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000000000', 0, '0.000000000000', '0.000000000000', 'Active', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(6, NULL, NULL, 'product1', 5, '<p>aaaaa</p>', '<p>aaaaaaaaaaaaaa</p>', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000000000', 0, '0.000000000000', '0.000000000000', 'Active', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, 'New product', 5, '<p>w</p>', '<p>ww</p>', 3, 2, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000000000', 0, '0.000000000000', '0.000000000000', 'Active', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, 'product3', 5, '<p>w</p>', '<p>ww</p>', 3, 2, 3, NULL, '../uploads/admin/products/16308498991.jpg', '../uploads/admin/products/16308498991.jpg', '../uploads/admin/products/16308498991.jpg', '../uploads/admin/products/16308498991.jpg', '../uploads/admin/products/16308499001.jpg', '../uploads/admin/products/16308499001.jpg', '0.000000000000', 0, '0.000000000000', '0.000000000000', 'Active', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, 'product4', 5, '<p>w</p>', '<p>ww</p>', 3, 2, 3, NULL, '../uploads/admin/products/16308500891.jpg', '../uploads/admin/products/16308500891.jpg', '../uploads/admin/products/16308500891.jpg', '../uploads/admin/products/16308500901.jpg', '../uploads/admin/products/16308500901.jpg', '../uploads/admin/products/16308500901.jpg', '600.000000000000', 0, '0.000000000000', '0.000000000000', 'Active', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, 'New product', 2, '<p>aaaaaaaaaaa</p>', '<p>aaaaaaaaaaaaaaaaaaaaaaaaaaa</p>', 1, 4, 1, NULL, '', '', '', '', '', '', '500.000000000000', 0, '234.000000000000', '12.000000000000', 'Active', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, 'product3', 5, '<p>eeeeeeeee</p>', '<p>eeeeeeeeeeeeeeeeeeeee</p>', 1, 4, 1, NULL, '', '', '', '', '', '', '400.000000000000', 0, '600.000000000000', '12.000000000000', 'Active', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2021-09-06', NULL, NULL, NULL, NULL),
(13, NULL, NULL, 'product1', 6, '<p>ttt</p>', '<p>tttttttttttttttttt</p>', 1, 4, 1, NULL, '', '', '', '', '', '', '900.000000000000', 0, '300.000000000000', '5.000000000000', 'Active', NULL, '2021-09-30', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2021-09-05', NULL, NULL, NULL, NULL),
(14, NULL, NULL, 'product4', 7, '<p>yyyyyy</p>', '<p>yyyyyyyyyyyyyyyy</p>', 1, 4, 1, NULL, '', '', '', '', '', '', '700.000000000000', 0, '300.000000000000', '5.000000000000', 'Active', NULL, '2021-09-30', NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2021-09-05', NULL, NULL, NULL, NULL),
(15, NULL, NULL, 'product5', 6, '<p>uuuuuu</p>', '<p>uuuuuuuuuuuuuuuuuuu</p>', 3, 2, 2, NULL, '../uploads/admin/products/16308528211.jpg', '../uploads/admin/products/16308528211.jpg', '../uploads/admin/products/16308528211.jfif', '../uploads/admin/products/16308528211.jpg', '../uploads/admin/products/16308528211.jpg', '../uploads/admin/products/16308528221.jpg', '1000.000000000000', 0, '500.000000000000', '1.000000000000', 'Inactive', NULL, '2021-09-30', NULL, NULL, NULL, NULL, 0, 2, 0, 9, '2021-09-05', NULL, NULL, NULL, 'www.youtube.come'),
(16, NULL, 'yyx', 'product6', 8, '<p>vvvvvvv</p>', '<p>vvvvvvvvvvvvvvvvvvvvvvvvv</p>', 3, 2, 3, NULL, '', '', '', '', '', '', '290.000000000000', 0, '120.000000000000', '4.000000000000', 'Active', NULL, '2021-09-30', NULL, NULL, NULL, NULL, 0, 1, 0, 10, '2021-09-05', NULL, NULL, NULL, 'www.youtube.com'),
(17, NULL, 'yyx', 'product6', 6, '<p>ccccccccc</p>', '<p>ccccccccccccccccccccccccc</p>', 1, 1, 1, NULL, '../uploads/admin/products/16309886441.jpg', '../uploads/admin/products/16309886441.jpg', '../uploads/admin/products/16309886451.jpg', '../uploads/admin/products/16309886461.jpg', '../uploads/admin/products/16309886461.jpg', '../uploads/admin/products/16309886471.jpg', '300.000000000000', 0, '200.000000000000', '10.000000000000', 'Active', NULL, '2021-09-30', NULL, NULL, NULL, NULL, 0, 1, 0, 5, '2021-09-07', NULL, NULL, NULL, 'www.youtube.com');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
CREATE TABLE IF NOT EXISTS `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplier_id`, `supplier_name`) VALUES
(1, 'Kamal'),
(2, 'Nimal');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_username` varchar(250) DEFAULT NULL,
  `u_name` varchar(250) DEFAULT NULL,
  `u_password` varchar(250) DEFAULT NULL,
  `u_otp` varchar(250) DEFAULT NULL,
  `u_email` varchar(250) DEFAULT NULL,
  `u_phone` varchar(100) DEFAULT NULL,
  `u_dob` varchar(50) DEFAULT NULL,
  `u_bank_name` varchar(250) DEFAULT NULL,
  `u_bank_account_no` varchar(100) DEFAULT NULL,
  `u_bank_branch` varchar(250) DEFAULT NULL,
  `u_type1_by` int(11) DEFAULT '0',
  `u_type2_by` int(11) DEFAULT '0',
  `u_type3_by` int(11) DEFAULT '0',
  `u_type4_by` int(11) DEFAULT '0',
  `u_type5_by` int(11) DEFAULT '0',
  `u_address` varchar(200) DEFAULT NULL,
  `u_country` varchar(255) DEFAULT NULL,
  `u_city` varchar(255) DEFAULT NULL,
  `u_img` varchar(255) DEFAULT NULL,
  `u_lang` varchar(10) DEFAULT NULL,
  `u_last_log` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `u_side` varchar(5) DEFAULT NULL,
  `u_currency` varchar(255) DEFAULT NULL,
  `u_upline` varchar(255) DEFAULT NULL,
  `u_register_by` int(11) DEFAULT NULL,
  `u_register_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `u_ref` int(11) DEFAULT '0',
  `u_updated_by` int(11) DEFAULT NULL,
  `u_updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `u_status` int(11) DEFAULT '1',
  PRIMARY KEY (`u_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

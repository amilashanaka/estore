<?php

require 'vendor/autoload.php';


include_once 'recepit_format.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

$options = new Options();
$options->set('defaultFont', 'Courier');
$options->set('isRemoteEnabled', TRUE);
$options->set('debugKeepTemp', TRUE);
$options->set('isHtml5ParserEnabled', true);
//$options->set('chroot', '');
$dompdf = new Dompdf($options);

// instantiate and use the dompdf class
$dompdf = new Dompdf();


$page=file_get_contents("format.html");

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A7', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream( 'file.pdf' , array( 'Attachment'=>0 ) );
<?php
include_once './top_header.php';
include_once './header.php';
?>
<div id="content">
<?php include_once './banner.php'; ?>
    <!-- End Content Top -->
    <?php include_once './new_arrival_product.php'; ?>
    <!-- End New Arrival Product -->
    <?php //include_once './banner_grid_adv.php'; ?>
    <!-- End Banner Grid Adv -->
    <?php include_once './new_products.php'; ?>
    <!-- End Slider Product Tab -->
    <?php include_once './list_designer.php'; ?>
    <!-- End List Designer -->
    <?php include_once './category_leading.php'; ?>
    <!-- End Category Leading -->
    <?php include_once './custom_services.php'; ?>
    <!-- End Custom Services -->
    <?php include_once './blog.php'; ?>
    <!-- End From our blog -->
</div>
<?php
include_once './footer.php';

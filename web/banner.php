	<div class="content-top">
			<div class="container">
				<div class="row">
					<div class="banner-simple-text">
						<h2>THE BIGGEST</h2>
						<h3>SALE OF THE day</h3>
						<div class="text-special">
							<strong>70</strong>
							<span>%<br/>OFF</span>
							<label>save up to</label>
							<a href="#">Shop women</a>
						</div>
					</div>
				</div>
                            <br>
                            <br>
				<div class="row">
					<div class="banner-box-adv">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="inner-box-banner-adv odd bottom-right">
									<a href="#" class="banner-adv-thumb-link"><img src="images/home_11/ad_01.png" alt="" /></a>
									<div class="text-adv-intro">
										<h2>Ring</h2>
										<h3>Collection</h3>
									</div>
									<div class="box-search-adv">
										
									</div>
									<a href="#" class="box-search-adv-link"><i class="fa fa-search"></i></a>
									<div class="text-adv-hidden">
										<p>Many students are cash conteos</p>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="inner-box-banner-adv even bottom-left">
									<a href="#" class="banner-adv-thumb-link"><img src="images/home_11/ad_02.png" alt="" /></a>
									<div class="text-adv-intro">
										<h2>Earrings</h2>
										<h3>Collections</h3>
									</div>
									<div class="box-search-adv">
										
									</div>
									<a href="#" class="box-search-adv-link"><i class="fa fa-search"></i></a>
									<div class="text-adv-hidden">
										<p>Many students are cash conteos</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="inner-box-banner-adv odd top-right">
									<a href="#" class="banner-adv-thumb-link"><img src="images/home_11/ad_03.png" alt="" /></a>
									<div class="text-adv-intro">
										<h2>Bracelet</h2>
										<h3>Collection</h3>
									</div>
									<div class="box-search-adv">
										
									</div>
									<a href="#" class="box-search-adv-link"><i class="fa fa-search"></i></a>
									<div class="text-adv-hidden">
										<p>Many students are cash conteos</p>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="inner-box-banner-adv even top-left">
									<a href="#" class="banner-adv-thumb-link"><img src="images/home_11/ad_04.png" alt="" /></a>
									<div class="text-adv-intro">
										<h2>Women's</h2>
										<h3>Necklace</h3>
									</div>
									<div class="box-search-adv">
										
									</div>
									<a href="#" class="box-search-adv-link"><i class="fa fa-search"></i></a>
									<div class="text-adv-hidden">
										<p>Many students are cash conteos</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
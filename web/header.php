<body>
	<header id="header">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div class="logo">
						<a href="#"><img src="images/home_11/logo.png" alt="" /></a>
					</div>
				</div>
				<div class="col-sm-8 col-xs-12">
					<div class="top-search">
						<div class="search-cat">
							<a href="#" class="box-cat-toggle">
								<img src="images/home_11/icon-cat-toggle.png" alt="" />
							</a>
							<div class="wrap-scrollbar" style="display: none;">
								<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 200px; height: 200px;">
									<div class="scrollbar" style="overflow: hidden; width: 200px; height: 200px;">
										<ul>
											<li class="level-0"><a href="#">All Categories</a></li>
											<li class="level-1"><a href="#">Default Category</a></li>
											<li class="level-2"><a href="#">Beauty &amp; Perfumes</a></li>
											<li class="level-2"><a href="#">Collection</a></li>
											<li class="level-2"><a href="#">Hugpo Dinp</a></li>
											<li class="level-2"><a href="#">Hat &amp; Cavar</a></li>
											<li class="level-2"><a href="#">Danlien Polosa</a></li>
											<li class="level-2"><a href="#">Chemise SLimFit</a></li>
										</ul>
									</div>
									<div class="slimScrollBar" style="background: none repeat scroll 0% 0% rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 68.9655px;"></div>
									<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: none repeat scroll 0% 0% rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
								</div>
							</div>
						</div>
						<div class="search-form">
							<form method="get">
								<input type="text" name="s" value="Search..." onfocus="if (this.value==this.defaultValue) this.value = ''" onblur="if (this.value=='') this.value = this.defaultValue"  />
								<input type="submit" value="" />
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="top-nav">
					<div class="row">
						<div class="col-md-10 col-sm-12 col-xs-12">
							<div class="main-nav">
								<ul class="list-inline">
									<li class="menu-item-has-children">
										<a href="#" class="active">Home</a>
										<ul class="sub-menu">
											<li><a href="#">Home 1</a></li>
											<li><a href="#">Home 2</a></li>
											<li><a href="#">Home 3</a></li>
											<li><a href="#">Home 4</a></li>
										</ul>
									</li>
									<li class="has-mega-menu">
										<a href="#">Corssbody</a>
										<ul class="sub-menu">
											<li>
												<div class="wrap-mega-menu">
													<div class="mega-menu-list-product">
														<h2 class="title-mega-menu">Collection</h2>
														<div class="wrap-item">
															<div class="item">
																<div class="inner-item">
																	<div class="mega-slide-thumb">
																		<a href="#"><img src="images/product_home11/1.jpg" alt="" /></a>
																	</div>
																	<div class="mega-slide-text">
																		<span>$45.99</span>
																		<del>$69.71</del>
																	</div>
																</div>
															</div>
															<div class="item">
																<div class="inner-item">
																	<div class="mega-slide-thumb">
																		<a href="#"><img src="images/product_home11/3.jpg" alt="" /></a>
																	</div>
																	<div class="mega-slide-text">
																		<span>$45.99</span>
																	</div>
																</div>
															</div>
															<div class="item">
																<div class="inner-item">
																	<div class="mega-slide-thumb">
																		<a href="#"><img src="images/product_home11/8.jpg" alt="" /></a>
																	</div>
																	<div class="mega-slide-text">
																		<span>$45.99</span>
																		<del>$69.71</del>
																	</div>
																</div>
															</div>
															<div class="item">
																<div class="inner-item">
																	<div class="mega-slide-thumb">
																		<a href="#"><img src="images/product_home11/9.jpg" alt="" /></a>
																	</div>
																	<div class="mega-slide-text">
																		<span>$45.99</span>
																	</div>
																</div>
															</div>
															<div class="item">
																<div class="inner-item">
																	<div class="mega-slide-thumb">
																		<a href="#"><img src="images/product_home11/10.jpg" alt="" /></a>
																	</div>
																	<div class="mega-slide-text">
																		<span>$45.99</span>
																		<del>$69.71</del>
																	</div>
																</div>
															</div>
															<div class="item">
																<div class="inner-item">
																	<div class="mega-slide-thumb">
																		<a href="#"><img src="images/product_home11/11.jpg" alt="" /></a>
																	</div>
																	<div class="mega-slide-text">
																		<span>$45.99</span>
																	</div>
																</div>
															</div>
															<div class="item">
																<div class="inner-item">
																	<div class="mega-slide-thumb">
																		<a href="#"><img src="images/product_home11/12.jpg" alt="" /></a>
																	</div>
																	<div class="mega-slide-text">
																		<span>$45.99</span>
																	</div>
																</div>
															</div>
															<div class="item">
																<div class="inner-item">
																	<div class="mega-slide-thumb">
																		<a href="#"><img src="images/product_home11/14.jpg" alt="" /></a>
																	</div>
																	<div class="mega-slide-text">
																		<span>$45.99</span>
																		<del>$69.71</del>
																	</div>
																</div>
															</div>
															<div class="item">
																<div class="inner-item">
																	<div class="mega-slide-thumb">
																		<a href="#"><img src="images/product_home11/18.jpg" alt="" /></a>
																	</div>
																	<div class="mega-slide-text">
																		<span>$45.99</span>
																		<del>$69.71</del>
																	</div>
																</div>
															</div>
															<div class="item">
																<div class="inner-item">
																	<div class="mega-slide-thumb">
																		<a href="#"><img src="images/product_home11/21.jpg" alt="" /></a>
																	</div>
																	<div class="mega-slide-text">
																		<span>$45.99</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="owl-direct-nav">
															<a class="prev" href="#"><i class="fa fa-arrow-circle-left"></i></a>
															<a class="next" href="#"><i class="fa fa-arrow-circle-right"></i></a>
														</div>
													</div>
													<!-- End Sub Mega Menu Slide-->
													<div class="mega-menu-simple-banner text-inner">
														<div class="mega-menu-simple-thumb">
															<a href="#"><img src="images/home_11/mega-menu-banner.png" alt="" /></a>
														</div>
														<div class="mega-menu-simple-text">
															<p class="simple-text1">big sale on canifa shop</p>
															<p class="simple-text2">up to<br/>80% off</p>
														</div>
													</div>
													<!-- End Mega Menu Simple Banner -->
												</div>
											</li>
										</ul>
									</li>
									<li class="has-mega-menu">
										<a href="#">Satchel Totes</a>
										<ul class="sub-menu">
											<li>
												<div class="wrap-mega-menu">
													<div class="row">
														<div class="col-md-6 col-sm-6 col-xs-12">
															<div class="mega-menu-simple-banner text-outer">
																<div class="mega-menu-simple-thumb">
																	<a href="#"><img src="images/home_11/mega-menu-women.png" alt="" /></a>
																</div>
																<div class="mega-menu-simple-text">
																	<p class="mega-menu-text-intro"><strong>Women fashion</strong>  <span>|  Lorem ipsum dolor sit amet</span></p>
																</div>
															</div>
															<!-- End Mega Menu Simple Banner -->
														</div>
														<div class="col-md-3 col-sm-3 col-xs-12">
															<div class="mega-menu-list-category">
																<h2>Categories</h2>
																<ul>
																	<li><a href="#">Tops</a></li>
																	<li><a href="#">Sweaters</a></li>
																	<li><a href="#">Bottoms</a></li>
																	<li><a href="#">Dresses</a></li>
																	<li><a href="#">Coats &amp; Jackets</a></li>
																	<li><a href="#">Scarves</a></li>
																	<li><a href="#">Pants</a></li>
																</ul>
															</div>
														</div>
														<div class="col-md-3 col-sm-3 col-xs-12">
															<div class="mega-menu-slider-brand">
																<h2>Style-Brands</h2>
																<div class="wrap-item">
																	<div class="item">
																		<div class="inner-brand">
																			<a href="#"><img src="images/banner/logo-brand-01.png" alt=""/></a>
																			<a href="#"><img src="images/banner/logo-brand-02.png" alt=""/></a>
																			<a href="#"><img src="images/banner/logo-brand-03.png" alt=""/></a>
																		</div>
																	</div>
																	<div class="item">
																		<div class="inner-brand">
																			<a href="#"><img src="images/banner/logo-brand-01.png" alt=""/></a>
																			<a href="#"><img src="images/banner/logo-brand-03.png" alt=""/></a>
																			<a href="#"><img src="images/banner/logo-brand-02.png" alt=""/></a>
																		</div>
																	</div>
																	<div class="item">
																		<div class="inner-brand">
																			<a href="#"><img src="images/banner/logo-brand-02.png" alt=""/></a>
																			<a href="#"><img src="images/banner/logo-brand-03.png" alt=""/></a>
																			<a href="#"><img src="images/banner/logo-brand-01.png" alt=""/></a>
																		</div>
																	</div>
																</div>
																<div class="owl-direct-nav">
																	<a class="prev" href="#"><i class="fa fa-arrow-circle-left"></i></a>
																	<a class="next" href="#"><i class="fa fa-arrow-circle-right"></i></a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</li>
										</ul>
									</li>
									<li><a href="#">Ladies Wallets</a></li>
									<li><a href="#">Sweatshirts</a></li>
									<li><a href="#">Jackets &amp; Coats</a></li>
								</ul>
							</div>
							<!-- End Nav -->
						</div>
						<div class="col-md-2 col-sm-12 col-xs-12">
							<div class="header-info">
								<div class="box-account-lc box">
									<a href="#" class="link-user-top"><img src="images/home_11/icon-user.png" alt="" /></a>
									<div class="box-inner">
										<ul class="links">
											<li class="first"><a href="#" title="My Account" class="top-link-myaccount">My Account</a></li>
											<li><a href="#" title="My Wishlist" class="top-link-wishlist">My Wishlist</a></li>
											<li><a href="#" title="Checkout" class="top-link-checkout">Checkout</a></li>
											<li class=" last"><a href="#" title="Log In" class="top-link-login">Log In</a></li>
										</ul>
										<div class="block block-language">
											<div class="lg-cur">
												<span>Language</span>
											</div>
											<ul>
												<li>
													<a class="selected" href="#">                    
													<img src="images/flags/flag-default.jpg" alt="flag">
													<span>English</span>
													</a>
												</li>
												<li>
													<a href="#">                    
													<img src="images/flags/flag-french.jpg" alt="flag">
													<span>French</span>
													</a>
												</li>
												<li>
													<a href="#">                    
													<img src="images/flags/flag-german.jpg" alt="flag">
													<span>German</span>
													</a>
												</li>
												<li>
													<a href="#">                    
													<img src="images/flags/flag-brazil.jpg" alt="flag">
													<span>Brazil</span>
													</a>
												</li>
											</ul>
										</div>
										<div class="block block-currency">
											<div class="item-cur">
												<span>Currency</span>           
											</div>
											<ul>
												<li>
													<a href="#"><span class="cur_icon">$</span> EUR</a>
												</li>
												<li>
													<a href="#"><span class="cur_icon">$</span> JPY</a>
												</li>
												<li>
													<a class="selected" href="#"><span class="cur_icon">$</span> USD</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="block-cart box">
									<a href="#" class="link-cart-top">
										<img src="images/home_11/icon-cart.png" alt="" />
										<sup class="number-cart-total">1</sup>
									</a>		
									<div class="block-content box-inner">
										<div class="inner">
											<p class="block-subtitle">Recently added item(s)</p>
											<ol id="cart-sidebar" class="mini-products-list">
												<li class="item odd">
													<a href="#" class="product-image">
													<img width="70" height="84" src="images/home_11/img-cart.gif" alt=""/>
													</a>
													<a href="" class="btn-remove">X</a>
													<a href="#" class="btn-edit">Edit</a>
													<div class="product-details">
														<p class="product-name">
															<a href="#">Bath Minerals and Salt</a>
														</p>
														<span class="price">$25.00</span>          
														<strong>1</strong> 
													</div>
												</li>
												<li class="item last even">
													<a href="#" class="product-image">
													<img width="70" height="84" src="images/home_11/img-cart.gif" alt=""/>
													</a>
													<a href="#" class="btn-remove">X</a>
													<a href="#" class="btn-edit">Edit</a>
													<div class="product-details">
														<p class="product-name">
															<a href="#">Large Camera Bag</a>
														</p>
														<span class="price">$20.00</span>                         
														<strong>1</strong> 
													</div>
												</li>
											</ol>
											<div class="summary">
												<p class="subtotal">
													<span class="label">Subtotal:</span> 
													<span class="price">$45.00</span>                                                                        
												</p>
											</div>
											<div class="actions">
												<div class="a-inner">
													<a class="btn-mycart" href="#">view my cart</a>
													<a href="#" class="btn-checkout">Checkout</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End Header Info -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- End Header -->
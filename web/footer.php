	<!-- End Content -->
	<footer id="footer">
		<div class="footer-newsletter" data-parallax="scroll" data-image-src="images/home_11/parallax_02.png">
			<div class="container">
				<div class="row">
					<div class="newsletter-intro pull-left">
						<span class="envalope-icon"><i class="fa fa-envelope-o"></i></span>
						<p>SUBSCRIBE TO OUR NEWSLETTER TO RECEIVE NEWS, <br/>UPDATES, AND ANOTHER STUFF BY EMAIL.</p>
					</div>
					<div class="newsletter-form pull-right">
						<form method="post">
							<input type="text" name="newsletter" value="Enter your email..." onfocus="if (this.value==this.defaultValue) this.value = ''" onblur="if (this.value=='') this.value = this.defaultValue"  />
							<input type="submit" value="subscribe" />
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- End Newsletter -->
		<div class="footer-quick-search">
			<div class="container">
				<div class="row">
					<div class="content-quick-search">
						<label>Quick link:</label>
						<a href="#">#</a>     
						<a href="#">A</a>        
						<a href="#">B</a>
						<a href="#">C</a>
						<a href="#">D</a>
						<a href="#">E</a>
						<a href="#">F</a>
						<a href="#">G</a>
						<a href="#">H</a>
						<a href="#">I</a>
						<a href="#">J</a>
						<a href="#">K</a>
						<a href="#">L</a>
						<a href="#">M</a>
						<a href="#">N</a>
						<a href="#">O</a>
						<a href="#">P</a>
						<a href="#">Q</a>
						<a href="#">R</a>
						<a href="#">S</a>
						<a href="#">U</a>
						<a href="#">V</a>
						<a href="#">W</a>
						<a href="#">X</a>
						<a href="#">Y</a>
						<a href="#">Z</a>
					</div>
				</div>
			</div>
		</div>
		<!-- End Quick Search -->
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-sm-3 col-xs-6">
						<div class="logo-footer"><a href="#"><img src="images/home_11/logo.png" alt="" /></a></div>
					</div>
					<div class="col-md-4 col-sm-3 col-xs-6">
						<div class="copy-right">
							<p>© 2015        <a href="#" class="privacy-policy">Privacy Policy</a></p>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<div class="contact-footer">
							<p>My Company , Glasgow D04 89GR<br/>Call us now: 800-2345-6789</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<div class="social-footer">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-linkedin"></i></a>
							<a href="#"><i class="fa fa-google-plus"></i></a>
							<a href="#"><i class="fa fa-rss"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- End Footer -->
</body>
</html>
<?php
include_once './top_header.php';
include_once 'data/data_product.php';

?>
<body class="hold-transition sidebar-mini">
<?php

if (isset($_GET['error'])) {
    $error = base64_decode($_GET['error']);

    if (isset($_GET['info'])) {

        $info = base64_decode($_GET['info']);


        echo '<script>  update_message('.$info.');</script>';
    }else{

        echo '<script>  error_by_code('.$error.');</script>';
    }


}

?>


<div class="wrapper">
    <!-- Navbar -->
    <?php include_once './navbar.php'; ?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include_once './sidebar.php'; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php
        $t1 = $lang['Product'];
        $t2 = $lang['Details'];
        if ($s_id == 0) {
            $t2 = $lang['New'] . " " . $t1;
        } else {

            $t2 = $lang['Update Product'];
        }
        include_once './page_header.php';
        ?>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">


                            <div class="card-body">
                                <div >
                                    <form action="data/register_product.php" class="templatemo-login-form" method="post" enctype="multipart/form-data" name="update_vehicles">
                                        <?php
                                        if ($p_id == 0) {

                                            echo '<input type="hidden" name="action" value="register">';
                                            echo '<input type="hidden" name="p_created_dt" value="' . $today . '">';
                                            echo '<input type="hidden" name="p_created_by" value="' . $user_act . '">';
                                        } else {

                                            echo ' <input type="hidden" name="action" value="update">';
                                            echo ' <input type="hidden" name="p_id" value="' . $p_id . '">';
                                            echo '<input type="hidden" name="p_updated_dt" value="' . $today . '">';
                                            echo '<input type="hidden" name="p_updated_by" value="' . $user_act . '">';
                                        }
                                        ?>


                                        <div class="col-lg-12 col-md-12 form-group">
                                            <div class="row">
                                               <div class="col-lg-4 col-md-4">
                                                    <div class="row form-group">
                                                        <div class="form-group" style="">
                                                            <div class="user_image">
                                                                <?php if ($row['p_img_1'] == '') { ?>
                                                                    <img name="p_img_1" id="p_img_1"  src="img/no_photo.png" class="bg-transparent profile_image" style="max-height:200px;width:auto">
                                                                <?php } else { ?>
                                                                    <img name="p_img_1" id="p_img_1"  src="<?= $row['p_img_1']; ?>" class="transparent profile_image" style="max-height:150px;width:auto">
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="input-group" >
                                                        <input type="file" name="p_img_file_1" id="p_img_file_1" class="form-control"  aria-describedby="inputGroupPrepend" style="display:none;align-content: center"/>
                                                        <input type="button" style="width: 150px;" value="Browse" id="browse_image_1" class="btn btn-block btn-success"/>
                                                    </div>
                                                </div> 
                                                <div class="col-lg-8 col-md-8 ">
                                                    <div class="row form-group">

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Product Name'] ?></label>
                                                            <input type="text" class="form-control" id="p_name"   name="p_name" value="<?php echo $row['p_name']; ?>"  required>
                                                        </div>


                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Product Cost'] ?></label>
                                                            <input type="number" step="0.01" class="form-control" id="p_cost" placeholder="0.00" name="p_cost" value="<?php echo $row['p_cost']; ?>" >
                                                        </div>


                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Product Stock'] ?></label>
                                                            <input type="number" step="1" class="form-control" id="p_stock" placeholder="0" name="p_stock" value="<?php echo ($row['p_stock']); ?>" >
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Product Discount'] ?></label>
                                                            <input type="number" step="0.01" class="form-control" id="p_discount" placeholder="0.00" name="p_discount" value="<?php echo  $row['p_discount']; ?>" >
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Expire Date'] ?></label>
                                                            <input type="date"   class="form-control" id="p_expire_date"   name="p_expire_date" value="<?php echo  $row['p_expire_date']; ?>" >
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Product Reference Code'] ?></label>
                                                            <input type="text" class="form-control" id="p_ref_code"   name="p_ref_code" value="<?php echo $row['p_ref_code']; ?>"  required>
                                                        </div>

                                                        
                                                       

                                                        
                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Select Brand'] ?></label>
                                                            <div>
                                                                <?php
                                                                    echo ' <select class="selectpicker form-control" name="p_brand" id="p_brand" value="'.$row['p_brand'].'"  data-live-search="true" required>';
                                                                    $database->loadAllBrands($row['p_brand']);
                                                                    echo '</select>';
                                                                 ?>
                                                                </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Select Category'] ?></label>
                                                                <div>
                                                                <?php
                                                                    echo ' <select class="selectpicker form-control" name="p_category" id="p_category" value="'.$row['p_category'].'"  data-live-search="true" required>';
                                                                    $database->loadAllCategories($row['p_category']);
                                                                    echo '</select>';
                                                                 ?>
                                                                </div>
                                                        </div>
                                                            
                                            
                                                        
                                            

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Set Available Quantity'] ?></label>
                                                            <input type="number" class="form-control" id="p_qty"   name="p_qty" value="<?php echo $row['p_qty']; ?>"  required>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Select Collection'] ?></label>
                                                                <div>
                                                                <?php
                                                                    echo ' <select class="selectpicker form-control" name="p_collection" id="p_collection"  value="'.$row['p_collection'].'" data-live-search="true" required>';
                                                                    $database->loadAllCollections($row['p_collection']);
                                                                    echo '</select>';
                                                                 ?>
                                                                </div>
                                                        </div>
                                                            

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Supplier Name'] ?></label>
                                                                <div>
                                                                <?php
                                                                    echo ' <select class="selectpicker form-control" name="p_supplier" id="p_supplier" value="'.$row['p_supplier'].'"  data-live-search="true" required>';
                                                                    $database->loadAllSuppliers($row['p_supplier']);
                                                                    echo '</select>';
                                                                 ?>
                                                                </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Sales Price'] ?></label>
                                                            <input type="number" class="form-control" id="p_sales_price"   name="p_sales_price" value="<?php echo $row['p_sales_price']; ?>"  required>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 form-group">                  
                                                            <label><?= $lang['Product Status'] ?></label>
                                                            <div>
                                                                <select class="form-control" name="p_status" id="p_status">
                                                                    <?php
                                                                        if ($row['p_status'] != '') {
                                                                        echo '<option selected=' . $row['p_status'] . '>' . $row['p_status'] . '</option>';
                                                                        }
                                                                    ?>
                                                                    <option value="Active">Active</option>
                                                                    <option value="Inactive">Inactive</option>

                                                                </select>
                                                            </div>
                                                      
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Stock Date'] ?></label>
                                                            <input type="date" class="form-control" id="p_stock_date"   name="p_stock_date" value="<?php echo $row['p_stock_date']; ?>"  required>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 form-group">
                                                            <label><?= $lang['Vidoe Url(Youtube/Vimeo)'] ?></label>
                                                            <input type="text" class="form-control" id="p_video"   name="p_video" value="<?php echo $row['p_video']; ?>"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        </div>
                                       

                                        <table>
                                            <tr >
                                                <td class="col-md-2.1">
                                                    <?php if ($row['p_img_2'] == '') { ?>
                                                        <img name="p_img_2" id="p_img_2"  src="img/no_photo.png" class="bg-transparent profile_image" style="max-height:130px;width:auto">
                                                        <?php } else { ?>
                                                        <img name="p_img_2" id="p_img_2"  src="<?= $row['p_img_2']; ?>" class="transparent profile_image" style="max-height:130px;width:auto">
                                                    <?php } ?>
                                                </td>
                                                <td class="col-md-2.1">
                                                    <?php if ($row['p_img_3'] == '') { ?>
                                                        <img name="p_img_3" id="p_img_3"  src="img/no_photo.png" class="bg-transparent profile_image" style="max-height:130px;width:auto">
                                                        <?php } else { ?>
                                                        <img name="p_img_3" id="p_img_3"  src="<?= $row['p_img_3']; ?>" class="transparent profile_image" style="max-height:130px;width:auto">
                                                    <?php } ?></td>
                                                    <td class="col-md-2.1">
                                                    <?php if ($row['p_img_4'] == '') { ?>
                                                        <img name="p_img_4" id="p_img_4"  src="img/no_photo.png" class="bg-transparent profile_image" style="max-height:130px;width:auto">
                                                        <?php } else { ?>
                                                        <img name="p_img_4" id="p_img_4"  src="<?= $row['p_img_4']; ?>" class="transparent profile_image" style="max-height:130px;width:auto">
                                                    <?php } ?>
                                                </td>
                                                <td class="col-md-2.1">
                                                    <?php if ($row['p_img_5'] == '') { ?>
                                                        <img name="p_img_5" id="p_img_5"  src="img/no_photo.png" class="bg-transparent profile_image" style="max-height:130px;width:auto">
                                                        <?php } else { ?>
                                                        <img name="p_img_5" id="p_img_5"  src="<?= $row['p_img_5']; ?>" class="transparent profile_image" style="max-height:130px;width:auto">
                                                    <?php } ?>
                                                </td>
                                                <td class="col-md-2.1">
                                                    <?php if ($row['p_img_6'] == '') { ?>
                                                        <img name="p_img_6" id="p_img_6"  src="img/no_photo.png" class="bg-transparent profile_image" style="max-height:130px;width:auto">
                                                        <?php } else { ?>
                                                        <img name="p_img_6" id="p_img_6"  src="<?= $row['p_img_6']; ?>" class="transparent profile_image" style="max-height:130px;width:auto">
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    <input type="file" name="p_img_file_2" id="p_img_file_2" class="form-control"  aria-describedby="inputGroupPrepend" style="display:none;align-content: center"/>
                                                    <input type="button" style="width: 100px;" value="Browse" id="browse_image_2" class="btn btn-block btn-success"/>
                                                   
                                                </td>
                                                <td >
                                                    <input type="file" name="p_img_file_3" id="p_img_file_3" class="form-control"  aria-describedby="inputGroupPrepend" style="display:none;align-content: center"/>
                                                    <input type="button" style="width: 100px;" value="Browse" id="browse_image_3" class="btn btn-block btn-success"/>
                                                </td>
                                                <td >
                                                    <input type="file" name="p_img_file_4" id="p_img_file_4" class="form-control"  aria-describedby="inputGroupPrepend" style="display:none;align-content: center"/>
                                                    <input type="button" style="width: 100px;" value="Browse" id="browse_image_4" class="btn btn-block btn-success"/>
                                                </td>
                                                <td >
                                                    <input type="file" name="p_img_file_5" id="p_img_file_5" class="form-control"  aria-describedby="inputGroupPrepend" style="display:none;align-content: center"/>
                                                    <input type="button" style="width: 100px;" value="Browse" id="browse_image_5" class="btn btn-block btn-success"/>
                                                </td>
                                                <td >
                                                    <input type="file" name="p_img_file_6" id="p_img_file_6" class="form-control"  aria-describedby="inputGroupPrepend" style="display:none;align-content: center"/>
                                                    <input type="button" style="width: 100px;" value="Browse" id="browse_image_6" class="btn btn-block btn-success"/>
                                                </td>
                                            </tr>
                                            </table>
                                            <br><br>
                                            


                                        <h5 class="text-divider"><span><?= $lang['Details'] ?></span></h5>
                                        <div class="row form-group">
                                            <div class="col-lg-12 col-md-12 form-group">
                                                <label><?= $lang['Short Description'] ?> :</label>
                                                <textarea type="text" class="form-control  summernote" id="P_short_des"  name="P_short_des" value="<?php echo $row['P_short_des']; ?>" ><?php echo $row['P_short_des']; ?></textarea>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-lg-12 col-md-12 form-group">
                                                <label><?= $lang['Detail Description'] ?> :</label>
                                                <textarea type="text" class="form-control  summernote" id="p_long_des"  name="p_long_des" value="<?php echo $row['p_long_des']; ?>" ><?php echo $row['p_long_des']; ?></textarea>
                                            </div>
                                        </div>



                                        
                                        
                                       
                                       



                                        <div  class="row form-group">
                                            <div class="col-lg-2 col-md-2 form-group">


                                                <?php
                                                if ($p_id != '') {


                                                    echo '<button type="submit" class="btn btn-block btn-outline-success">Update Now</button>';
                                                } else {


                                                    echo '<button type="submit" class="btn btn-block btn-outline-secondary">ADD New</button>';
                                                }
                                                ?>



                                            </div>
                                            
                                            <div class="col-lg-2 col-md-2 form-group">
                                                <button type="reset" class="btn btn-block btn-outline-warning">Reset</button>
                                            </div>


                                        </div>

                                    </form>
                                </div>
                                <!-- /.tab-content -->
                            </div><!-- /.card-body -->
                        </div>






                    </div>

                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>


    <script>
        $('#browse_image_1').on('click', function (e) {

            $('#p_img_file_1').click();
        });
        $('#p_img_file_1').on('change', function (e) {
            var fileInput = this;
            if (fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#p_img_1').attr('src', e.target.result);
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        });


        $('#browse_image_2').on('click', function (e) {

            $('#p_img_file_2').click();
        });
        $('#p_img_file_2').on('change', function (e) {
            var fileInput = this;
            if (fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#p_img_2').attr('src', e.target.result);
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        });


        $('#browse_image_3').on('click', function (e) {

            $('#p_img_file_3').click();
        });
        $('#p_img_file_3').on('change', function (e) {
            var fileInput = this;
            if (fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#p_img_3').attr('src', e.target.result);
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        });

    
        $('#browse_image_4').on('click', function (e) {

            $('#p_img_file_4').click();
        });
        $('#p_img_file_4').on('change', function (e) {
            var fileInput = this;
            if (fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#p_img_4').attr('src', e.target.result);
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        });

        $('#browse_image_5').on('click', function (e) {

            $('#p_img_file_5').click();
        });
        $('#p_img_file_5').on('change', function (e) {
            var fileInput = this;
            if (fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#p_img_5').attr('src', e.target.result);
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        });

    
        $('#browse_image_6').on('click', function (e) {

            $('#p_img_file_6').click();
        });
        $('#p_img_file_6').on('change', function (e) {
            var fileInput = this;
            if (fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#p_img_6').attr('src', e.target.result);
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        });

    </script>



    <script>


        $('#end_date').datetimepicker({

            defaultDate: new Date("<?php echo $row['enddate']; ?>"),

            format: 'YYYY-MM-DD',

            maxDate: moment()

        });


    </script>


    <!-- /.content-wrapper -->
    <?php include_once './footer.php'; ?>

</div>

</body>
</html>

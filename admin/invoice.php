<?php
include_once './top_header.php';
include_once 'data/data_invoice.php';

?>
<body class="hold-transition sidebar-mini">
<?php

if (isset($_GET['error'])) {
    $error = base64_decode($_GET['error']);

    if (isset($_GET['info'])) {

        $info = base64_decode($_GET['info']);


        echo '<script>  update_message("'.$info.'");</script>';
    }else{

        echo '<script>  error_by_code('.$error.');</script>';
    }


}

?>


<div class="wrapper">
    <!-- Navbar -->
    <?php include_once './navbar.php'; ?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include_once './sidebar.php'; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php
        $t1 = $lang['Invoice'];
        $t2 = $lang['Details'];
        if ($in_id == 0) {
            $t2 = $lang['New'] . " " . $t1;
        } else {

            $t2 = $lang['Update'].$lang['Invoice'];
        }
        include_once './page_header.php';
        ?>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">


                            <div class="card-body">
                                <div >
                                    <form action="data/register_invoice.php" class="templatemo-login-form" method="post" enctype="multipart/form-data" name="update_vehicles">
                                        <?php
                                        if ($in_id == 0) {

                                            echo '<input type="hidden" name="action" value="register">';
                                            echo '<input type="hidden" name="in_created_dt" value="' . $today . '">';
                                            echo '<input type="hidden" name="in_created_by" value="' . $user_act . '">';
                                        } else {

                                            echo '<input type="hidden" name="action" value="update">';
                                            echo '<input type="hidden" name="in_id" value="' . $in_id . '">';
                                            echo '<input type="hidden" name="in_updated_dt" value="' . $today . '">';
                                            echo '<input type="hidden" name="in_updated_by" value="' . $user_act . '">';
                                        }
                                        ?>

                                        <div class="row mb-3">
                                            <label for="in_name" class="col-sm-2 col-form-label">Invoice to</label>
                                            <div class="col-sm-10">
                                                <?php



                                                echo ' <select class="selectpicker form-control" name="u_id" id="u_id"   data-live-search="true" required>';
                                                echo ' <option  value=0> User Name </option>';
                                                $database->loadAllUsers($row['u_id']);
                                                echo '</select>';



                                                ?>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label for="in_contact" class="col-sm-2 col-form-label">Contact Number</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="in_contact">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="in_address" class="col-sm-2 col-form-label">Address</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="in_address">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="in_date" class="col-sm-2 col-form-label">Date</label>
                                            <div class="col-sm-10">
                                                <input type="date" class="form-control" name="in_date">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="in_desc" class="col-sm-2 col-form-label">Note</label>
                                            <div class="col-sm-10">
                                                <textarea type="text" class="form-control  summernote" id="in_desc"  name="in_desc" value="<?php echo $row['s_desc']; ?>" ><?php echo $row['s_desc']; ?></textarea>
                                            </div>
                                        </div>

                                        <button type="submit" style="float: right" class="btn btn-primary">Next</button>

                                    </form>
                                </div>
                                <!-- /.tab-content -->
                            </div><!-- /.card-body -->
                        </div>






                    </div>

                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>


    <script>
        $('#browse_image').on('click', function (e) {

            $('#pk_img_file').click();
        });
        $('#pk_img_file').on('change', function (e) {
            var fileInput = this;
            if (fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#pk_img').attr('src', e.target.result);
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        });

    </script>

    <script>


        $('#end_date').datetimepicker({

            defaultDate: new Date("<?php echo $row['enddate']; ?>"),

            format: 'YYYY-MM-DD',

            maxDate: moment()

        });


    </script>


    <!-- /.content-wrapper -->
    <?php include_once './footer.php'; ?>

</div>

</body>
</html>

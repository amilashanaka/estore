<?php
include_once './top_header.php';

include_once './data/data_list.php';
?>


<body class="hold-transition sidebar-mini">


    <?php
if (isset($_GET['error'])) {
    $error = base64_decode($_GET['error']);
    echo '<script>  error_by_code('.$error.');</script>';
}
?>

    <div class="wrapper">
        <!-- Navbar -->
        <?php include_once './navbar.php'; ?>
        <?php include_once './sidebar.php'; ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->

            <?php

        $t1 = $lang['BRAND'];
        $t2 = $lang['List'];

        include_once './page_header.php';

        ?>


            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-12">

                        <div class="card">

                            <div class="card-header">
                                <h3 class="card-title">
                                    <button type="button" class="btn btn-block  btn-outline-secondary"
                                        onclick="location.href = 'brand.php';"><?= $lang['Add New'] ?></button>

                                </h3>
                            </div>

                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example23"
                                    class="display nowrap table table-hover table-striped table-bordered"
                                    cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><?= $lang['Brand Name'] ?></th>
                                            <th><?= $lang['Category'] ?></th>
                                            <th><?= $lang['Logo'] ?></th>
                                            <th><?= $lang['Action'] ?></th>
                                            


                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th><?= $lang['Brand Name'] ?></th>
                                            <th><?= $lang['Category'] ?></th>
                                            <th width="200px;"><?= $lang['Logo'] ?></th>
                                            <th width="200px;"><?= $lang['Action'] ?></th>

                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                                $i = 1;
                                                while ($row = mysqli_fetch_assoc($result_brand_list)) {


                                                    ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><a
                                                    href="brand.php?brand_id=<?= base64_encode($row['brand_id']) ?>"><?= $row['brand_name'] ?></a>
                                            </td>
                                            <td><?= $row['brand_category'] ?></td>
                                            <td><img src="<?php echo $row['brand_img']; ?>" width="80px;" height="auto"></td>
                                            <td class="row form-group">&nbsp &nbsp &nbsp<div><button type="button" class="btn btn-block btn-success" onclick="location.href='brand.php?action=update&amp;brand_id=<?= base64_encode($row['brand_id']) ?>';" style="width:100px;">Update</button></div>&nbsp&nbsp<div><button type="button" id="btnln84" class="btn btn-block btn-danger" onclick="deactivateProduct('84','pd');" style="width:100px;">Delete</button></div></td>
                                            

                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>

        </div>
        <?php include_once './control-sidebar.php'; ?>
        <!-- /.content-wrapper -->
        <?php include_once './footer.php'; ?>

    </div>
    <!-- ./wrapper -->
</body>

</html>
<?php
include_once './top_header.php';
include_once 'data/data_category.php';

?>

<body class="hold-transition sidebar-mini">
    <?php

if (isset($_GET['error'])) {
    $error = base64_decode($_GET['error']);

    if (isset($_GET['info'])) {

        $info = base64_decode($_GET['info']);


        echo '<script>  update_message('.$info.');</script>';
    }else{

        echo '<script>  error_by_code('.$error.');</script>';
    }


}

?>


    <div class="wrapper">
        <!-- Navbar -->
        <?php include_once './navbar.php'; ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?php include_once './sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <?php
        $t1 = $lang['Category'];
        $t2 = $lang['Details'];
        if ($s_id == 0) {
            $t2 = $lang['New'] . " " . $t1;
        } else {

            $t2 = $lang['Update Category'];
        }
        include_once './page_header.php';
        ?>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">


                                <div class="card-body">
                                    <div>
                                        <form action="data/register_category.php" class="templatemo-login-form"
                                            method="post" enctype="multipart/form-data" name="update_category">
                                            <?php
                                        if ($category_id == 0) {

                                            echo '<input type="hidden" name="action" value="register">';
                                            echo '<input type="hidden" name="cat_created_dt" value="' . $today . '">';
                                            // echo '<input type="hidden" name="cat_created_by" value="' . $user_act . '">';
                                        } else {

                                            echo ' <input type="hidden" name="action" value="update">';
                                            echo ' <input type="hidden" name="category_id" value="' . $category_id . '">';
                                            // echo '<input type="hidden" name="p_updated_dt" value="' . $today . '">';
                                            // echo '<input type="hidden" name="p_updated_by" value="' . $user_act . '">';
                                        }
                                        ?>


                                            <div class="col-lg-12 col-md-12 form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="row form-group">
                                                            <div class="form-group" style="">
                                                                <div class="user_image">
                                                                    <?php if ($row['cat_img'] == '') { ?>
                                                                    <img name="cat_img" id="cat_img"
                                                                        src="img/no_photo.png"
                                                                        class="bg-transparent profile_image"
                                                                        style="max-height:200px;width:auto">
                                                                    <?php } else { ?>
                                                                    <img name="cat_img" id="cat_img"
                                                                        src="<?= $row['cat_img']; ?>"
                                                                        class="transparent profile_image"
                                                                        style="max-height:150px;width:auto">
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="input-group">
                                                            <input type="file" name="cat_img_file" id="cat_img_file"
                                                                class="form-control"
                                                                aria-describedby="inputGroupPrepend"
                                                                style="display:none;align-content: center" />
                                                            <input type="button" style="width: 150px;" value="Browse"
                                                                id="browse_image" class="btn btn-block btn-success" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 ">
                                                        <div class="row form-group">

                                                            <div class="col-lg-6 col-md-6 form-group">
                                                                <label><?= $lang['Category Name'] ?></label>
                                                                <input type="text" class="form-control"
                                                                    id="category_name" name="category_name"
                                                                    value="<?php echo $row['category_name']; ?>"
                                                                    required>
                                                            </div>


                                                            <div class="col-lg-6 col-md-6 form-group">
                                                                <label><?= $lang['Status'] ?></label>
                                                                <div>
                                                                    <select class="form-control" name="status"
                                                                        id="status">
                                                                        <?php
                                                                        if ($row['status'] != '') {
                                                                        echo '<option selected=' . $row['status'] . '>' . $row['status'] . '</option>';
                                                                        }
                                                                    ?>
                                                                        <option value="Active">Active</option>
                                                                        <option value="Inactive">Inactive</option>

                                                                    </select>
                                                                </div>

                                                            </div>


                                                            <div class="col-lg-6 col-md-6 form-group">
                                                                <label><?= $lang['Category Level'] ?></label>
                                                                <input type="number" step="1" class="form-control"
                                                                    id="level" placeholder="0" name="level"
                                                                    value="<?php echo ($row['level']); ?>">
                                                            </div>


                                                            <div class="col-lg-6 col-md-6 form-group">
                                                                <label><?= $lang['Main Category'] ?></label>
                                                                <div>
                                                                    <select class="form-control" name="main_cat"
                                                                        id="main_cat">
                                                                        <?php
                                                                        if ($row['main_cat'] != '') {
                                                                        echo '<option selected=' . $row['main_cat'] . '>' . $row['main_cat'] . '</option>';
                                                                        }
                                                                    ?>
                                                                        <option value="Jewelery">Jewelery</option>
                                                                        <option value="Sweat Shirts">Sweat Shirts
                                                                        </option>
                                                                        <option value="Clothing">Clothing</option>
                                                                        <option value="Dresses">Dresses</option>
                                                                        <option value="Electronics">Electronics</option>

                                                                    </select>
                                                                </div>

                                                            </div>

                                                            </div>
                                                    </div>
                                                                    </div>
                                                    <br><br>


                                                    

                                                    <h5 class="text-divider"><span><?= $lang['Details'] ?></span></h5>
                                                    <div class="row form-group">
                                                        <div class="col-lg-12 col-md-12 form-group">
                                                            <label><?= $lang['Description'] ?> :</label>
                                                            <textarea type="text" class="form-control  summernote"
                                                                id="description" name="description"
                                                                value="<?php echo $row['description']; ?>"><?php echo $row['description']; ?></textarea>
                                                        </div>
                                                    </div>

                                                   


                                                   





                                                    <div class="row form-group">
                                                        <div class="col-lg-2 col-md-2 form-group">


                                                            <?php
                                                if ($category_id != '') {

                                                   
                                                    echo '<button type="submit" class="btn btn-block btn-outline-success">Update Now</button>';
                                                    
                                                } else {


                                                    echo '<button type="submit" class="btn btn-block btn-outline-secondary">ADD New</button>';
                                                }
                                                ?>
                                                


                                                        </div>

                                                        <div class="col-lg-2 col-md-2 form-group">
                                                            <button type="reset"
                                                                class="btn btn-block btn-outline-warning">Reset</button>
                                                        </div>


                                                    </div>
                                               
                                        </form>
                                    </div>
                                    <!-- /.tab-content -->
                                </div><!-- /.card-body -->
                            






                        </div>

                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>


        <script>
        $('#browse_image').on('click', function(e) {

            $('#cat_img_file').click();
        });
        $('#cat_img_file').on('change', function(e) {
            var fileInput = this;
            if (fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#cat_img').attr('src', e.target.result);
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        });
        </script>



        <script>
        $('#end_date').datetimepicker({

            defaultDate: new Date("<?php echo $row['enddate']; ?>"),

            format: 'YYYY-MM-DD',

            maxDate: moment()

        });
        </script>


        <!-- /.content-wrapper -->
        <?php include_once './footer.php'; ?>

    </div>

</body>

</html>
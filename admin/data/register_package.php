<?php

 include_once '../../conn.php';
 include_once '../../inc/functions.php';
 include_once '../../inc/imageUpload.php';

 //Fetching Values from URL
if (isset($_POST['pk_id'])) { $pk_id= $_POST['pk_id'];}else{ $pk_id= 0;}
if (isset($_POST['pk_name'])) { $pk_name= $_POST['pk_name'];}else{ $pk_name= ' ';}
if (isset($_POST['pk_des'])) { $pk_des= $_POST['pk_des'];}else{ $pk_des= '';}
if (isset($_POST['pk_amount'])) { $pk_amount= $_POST['pk_amount'];}else{ $pk_amount= 0;}
if (isset($_POST['pk_cost'])) { $pk_cost= $_POST['pk_cost'];}else{ $pk_cost= '';}
if (isset($_POST['pk_discount'])) { $pk_discount= $_POST['pk_discount'];}else{ $pk_discount= 0;}


if (isset($_POST['pk_created_by'])) { $s_created_by= $_POST['pk_created_by'];}else{ $s_created_by= 0;}
if (isset($_POST['s_created_dt'])) { $s_created_dt= $_POST['s_created_dt'];}else{ $s_created_dt= '';}
if (isset($_POST['s_updated_by'])) { $s_updated_by= $_POST['s_updated_by'];}else{ $s_updated_by= 0;}
if (isset($_POST['s_updated_dt'])) { $s_updated_dt= $_POST['s_updated_dt'];}else{ $s_updated_dt= null;}
if (isset($_POST['s_status'])) { $s_status= $_POST['s_status'];}else{ $s_status= 0;}


//Action 
$action = $_POST['action'];
 
// image location 
$target_dir = "../../uploads/admin/packages/";
$targ_front="../uploads/admin/packages/";
$tmp =getResizeImg("pk_img_file", $target_dir,422,552);


if($tmp!=''){

    $pk_img=$targ_front.$tmp;
}else{
    $pk_img='';
}



     
if ($action == 'register') {

    if ($pk_name != '') {

        $sql_check = "SELECT * FROM packages WHERE pk_name='" . $pk_name . "'";
        $result = mysqli_query($conn, $sql_check);



        if (mysqli_num_rows($result) > 0) {
            header('Location: ../service.php?error=' . base64_encode(10));
        } else {

            if (add_services($s_name,$s_img, $s_desc, $s_amount, $s_cost, $s_discount,$s_created_by, $s_created_dt, $s_status,$conn)) {

                header('Location: ../service_list.php?error=' . base64_encode(4));
            } else {
                header('Location: ../service.php?error=' . base64_encode(3));
            }
        }
    } else {
        header('Location: ../service.php?error=' . base64_encode(3));
    }
}



if ($action == 'update' && $s_id > 0) {


    $result= update_services($s_id,$s_name,$s_img, $s_desc, $s_amount, $s_cost, $s_discount,$s_updated_by, $s_updated_dt, $s_status,$conn);

    $result=implode(" ",$result);

    if ($result!=null) {

        header('Location: ../service.php?s_id=' . base64_encode($s_id) . '&error=' . base64_encode(1).'&info='.  base64_encode($result));

    } else {
        header('Location: ../service.php?s_id=' . base64_encode($s_id) . '&error=' . base64_encode(3));

    }
}


 
 
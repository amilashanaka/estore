<?php

 include_once '../../conn.php';
 include_once '../../inc/functions.php';
 include_once '../../inc/imageUpload.php';

 //Fetching Values from URL
if (isset($_POST['category_id'])) { $category_id= $_POST['category_id'];}else{ $category_id= 0;}
if (isset($_POST['category_name'])) { $category_name= $_POST['category_name'];}else{ $category_name= ' NULL';}
if (isset($_POST['description'])) { $description= $_POST['description'];}else{ $description= '';}
if (isset($_POST['status'])) { $status= $_POST['status'];}else{ $status= '';}
if (isset($_POST['main_cat'])) { $main_cat= $_POST['main_cat'];}else{ $main_cat= 'NULL';}
if (isset($_POST['level'])) { $level= $_POST['level'];}else{ $level= '0';}
if (isset($_POST['cat_img'])) { $cat_img= $_POST['cat_img'];}else{ $cat_img= 'NULL';}

                                           

//Action 
$action = $_POST['action'];
// var_dump($action);
// exit();
 
// image location 
$target_dir = "../../uploads/admin/categories/";
$targ_front="../uploads/admin/categories/";


$tmp=getResizeImg("cat_img_file", $target_dir,422,552);


if($tmp!=''){

    $cat_img=$targ_front.$tmp;
}else{
    $cat_img='';
}


     
if ($action == 'register') {


            if ( add_category( $category_name, $main_cat, $level, $status, $description,$cat_img, $conn)) {

                header('Location: ../category_list.php?error=' . base64_encode(4));
            } else {
                header('Location: ../category.php?error=' . base64_encode(3));
            }

}



if ($action == 'update' && $category_id > 0) {
    

    $result= update_category($category_id, $category_name, $main_cat, $level, $status, $description, $cat_img, $conn);
   
    $result=implode(" ",$result);
    
    if ($result!=null) {
        
        header('Location: ../category.php?category_id=' . base64_encode($category_id) . '&error=' . base64_encode(1).'&info='.  base64_encode($result));
        
    } else {
        header('Location: ../category.php?category_id=' . base64_encode($category_id) . '&error=' . base64_encode(3));

    }
}


 
 
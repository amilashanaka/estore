<?php

 include_once '../../conn.php';
 include_once '../../inc/functions.php';
 include_once '../../inc/imageUpload.php';

 //Fetching Values from URL
if (isset($_POST['brand_id'])) { $brand_id= $_POST['brand_id'];}else{ $brand_id= 0;}
if (isset($_POST['brand_name'])) { $brand_name= $_POST['brand_name'];}else{ $brand_name= ' NULL';}
if (isset($_POST['brand_desc'])) { $brand_desc= $_POST['brand_desc'];}else{ $brand_desc= '';}
if (isset($_POST['b_status'])) { $b_status= $_POST['b_status'];}else{ $b_status= '';}
if (isset($_POST['brand_category'])) { $brand_category= $_POST['brand_category'];}else{ $brand_category= 'NULL';}
if (isset($_POST['b_rank'])) { $b_rank= $_POST['b_rank'];}else{ $b_rank= '0';}
if (isset($_POST['brand_img'])) { $brand_img= $_POST['brand_img'];}else{ $brand_img= 'NULL';}

                                           

//Action 
$action = $_POST['action'];
// var_dump($action);
// exit();
 
// image location 
$target_dir = "../../uploads/admin/brands/";
$targ_front="../uploads/admin/brands/";


$tmp=getResizeImg("brand_img_file", $target_dir,422,552);


if($tmp!=''){

    $brand_img=$targ_front.$tmp;
}else{
    $brand_img='';
}


     
if ($action == 'register') {


            if ( add_brand( $brand_name, $brand_category, $b_rank, $b_status, $brand_desc, $brand_img, $conn)) {

                header('Location: ../brand_list.php?error=' . base64_encode(4));
            } else {
                header('Location: ../brand.php?error=' . base64_encode(3));
            }

}



if ($action == 'update' && $brand_id > 0) {
    

    $result= update_brand($brand_id, $brand_name, $brand_category, $b_rank, $b_status, $brand_desc, $brand_img, $conn);
   
    $result=implode(" ",$result);
    
    if ($result!=null) {
        
        header('Location: ../brand.php?brand_id=' . base64_encode($brand_id) . '&error=' . base64_encode(1).'&info='.  base64_encode($result));
        
    } else {
        header('Location: ../brand.php?brand_id=' . base64_encode($brand_id) . '&error=' . base64_encode(3));

    }
}


 
 
<?php

 include_once '../../conn.php';
 include_once '../../inc/functions.php';
 include_once '../../inc/imageUpload.php';

 //Fetching Values from URL
if (isset($_POST['p_id'])) { $p_id= $_POST['p_id'];}else{ $p_id= 0;}
if (isset($_POST['p_name'])) { $p_name= $_POST['p_name'];}else{ $p_name= ' NULL';}
if (isset($_POST['P_short_des'])) { $P_short_des= $_POST['P_short_des'];}else{ $P_short_des= '';}
if (isset($_POST['p_long_des'])) { $p_long_des= $_POST['p_long_des'];}else{ $p_long_des= '';}
if (isset($_POST['p_qty'])) { $p_qty= $_POST['p_qty'];}else{ $p_qty= '0';}
if (isset($_POST['p_cost'])) { $p_cost= $_POST['p_cost'];}else{ $p_cost= '0';}
if (isset($_POST['p_discount'])) { $p_discount= $_POST['p_discount'];}else{ $p_discount= 0;}
if (isset($_POST['p_expire_date'])) { $p_expire_date= $_POST['p_expire_date'];}else{ $p_expire_date= null;}
if (isset($_POST['p_stock'])) { $p_stock= $_POST['p_stock'];}else{ $p_stock= '0';}
if (isset($_POST['p_ref_code'])) { $p_ref_code= $_POST['p_ref_code'];}else{ $p_ref_code= null;}
if (isset($_POST['p_category'])) { $p_category= $_POST['p_category'];}else{ $p_category= '';}
if (isset($_POST['p_collection'])) { $p_collection= $_POST['p_collection'];}else{ $p_collection= null;}
if (isset($_POST['p_supplier'])) { $p_supplier= $_POST['p_supplier'];}else{ $p_supplier= null;}
if (isset($_POST['p_sales_price'])) { $p_sales_price= $_POST['p_sales_price'];}else{ $p_sales_price= '';}
if (isset($_POST['p_brand'])) { $p_brand= $_POST['p_brand'];}else{ $p_brand= '';}
if (isset($_POST['p_stock_date'])) { $p_stock_date= $_POST['p_stock_date'];}else{ $p_stock_date= 0;}
if (isset($_POST['p_video'])) { $p_video= $_POST['p_video'];}else{ $p_video= '';}
if (isset($_POST['p_img_1'])) { $p_img_1= $_POST['p_img_1'];}else{ $p_img_1= '';}
if (isset($_POST['p_img_2'])) { $p_img_2= $_POST['p_img_2'];}else{ $p_img_2= '';}
if (isset($_POST['p_img_3'])) { $p_img_3= $_POST['p_img_3'];}else{ $p_img_3= '';}
if (isset($_POST['p_img_4'])) { $p_img_4= $_POST['p_img_4'];}else{ $p_img_4= '';}
if (isset($_POST['p_img_5'])) { $p_img_5= $_POST['p_img_5'];}else{ $p_img_5= '';}
if (isset($_POST['p_img_6'])) { $p_img_6= $_POST['p_img_6'];}else{ $p_img_6= '';}
if (isset($_POST['p_img_6'])) { $p_img_6= $_POST['p_img_6'];}else{ $p_img_6= '';}
if (isset($_POST['p_status'])) { $p_status= $_POST['p_status'];}else{ $p_status= '';}


//Action 
$action = $_POST['action'];

 
// image location 
$target_dir = "../../uploads/admin/products/";
$targ_front="../uploads/admin/products/";


$tmp_1=getResizeImg("p_img_file_1", $target_dir,422,552);


if($tmp_1!=''){

    $p_img_1=$targ_front.$tmp_1;
}else{
    $p_img_1='';
}


$tmp_2=getResizeImg("p_img_file_2", $target_dir,422,552);


if($tmp_2!=''){

    $p_img_2=$targ_front.$tmp_2;
}else{
    $p_img_2='';
}



$tmp_3=getResizeImg("p_img_file_3", $target_dir,422,552);


if($tmp_3!=''){

    $p_img_3=$targ_front.$tmp_3;
}else{
    $p_img_3='';
}


$tmp_4=getResizeImg("p_img_file_4", $target_dir,422,552);


if($tmp_4!=''){

    $p_img_4=$targ_front.$tmp_4;
}else{
    $p_img_4='';
}



$tmp_5=getResizeImg("p_img_file_5", $target_dir,422,552);


if($tmp_5!=''){

    $p_img_5=$targ_front.$tmp_5;
}else{
    $p_img_5='';
}


$tmp_6=getResizeImg("p_img_file_6", $target_dir,422,552);


if($tmp_6!=''){

    $p_img_6=$targ_front.$tmp_6;
}else{
    $p_img_6='';
}



     
if ($action == 'register') {


            if ( add_product( $p_name,$p_qty, $P_short_des, $p_long_des, $p_category, $p_brand, $p_collection, $p_img_1, $p_img_2, $p_img_3, $p_img_4, $p_img_5, $p_img_6, $p_sales_price, $p_cost, $p_discount,$p_status, $p_stock_date, $p_expire_date, $p_supplier, $p_stock, $p_ref_code, $p_video,$conn)) {

                header('Location: ../product_list.php?error=' . base64_encode(4));
            } else {
                header('Location: ../product.php?error=' . base64_encode(3));
            }

}



if ($action == 'update' && $p_id > 0) {


    $result= update_product($p_id, $p_name,$p_qty, $P_short_des, $p_long_des, $p_category, $p_brand, $p_collection, $p_img_1, $p_img_2, $p_img_3, $p_img_4, $p_img_5, $p_img_6, $p_sales_price, $p_cost, $p_discount,$p_status, $p_stock_date, $p_expire_date, $p_supplier, $p_stock, $p_ref_code, $p_video,$conn);
    
    $result=implode(" ",$result);
    
    if ($result!=null) {
        
        header('Location: ../product.php?p_id=' . base64_encode($p_id) . '&error=' . base64_encode(1).'&info='.  base64_encode($result));
        
    } else {
        header('Location: ../product.php?p_id=' . base64_encode($p_id) . '&error=' . base64_encode(3));

    }
}


 
 